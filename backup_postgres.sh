#!/bin/bash
#
# A daily backup for your docker postgresql database, run it daily
# It will cycle with the day of week (1..7)
#
DT=`date +%w`

### You will need to adjust DST and DOCKER_ENV Paths ###
# Backup destination directory
DST=/usr/local/backup
# Docker env file from Misskey
DOCKER_ENV=/usr/local/misskey/data/.config/docker.env
###

source $DOCKER_ENV || exit 1

mkdir -p $DST
cd $DST || exit 1

# Get the posgres container ID
pgcont=`get_container_id.sh postgres` || exit 1

# Dump quick, compress later
docker exec -i $pgcont pg_dump -U $POSTGRES_USER $POSTGRES_DB > backup-$POSTGRES_DB.$DT.pgsql

nice xz --force backup-$POSTGRES_DB.$DT.pgsql

exit $?
