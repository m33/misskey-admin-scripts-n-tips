#!/bin/bash
#
# semi automatic misskey updates
# This script may trash your instance, use with caution, make backups...
#

### You will need to adjust Paths ###
# Store containers log
LOG=/var/log/docker/misskey.log
# Misskey (git) code directory
CODE=/usr/local/misskey/code
# Misskey (containers volumes) data directory
DATA=/usr/local/misskey/data
# Docker compose file name
COMPOSE=docker-compose.yml
# Backup destination directory
DST=/usr/local/backup
# Automatically stop, update and start the running instance
AUTOUPDATE=1
TMP=/tmp/.update_misskey.$$
###

if [ ! -d $CODE ] || [ ! -d $DATA ]; then
  echo "Error, you may need to adjust path in this script"
  exit 1
fi

# Take some care before upgrading
echo "Backing up current code... (you really should have done yourself a data backup before upgrading, something like backup_postgres.sh)"
echo "$CODE will be archived in $DST/backup-code.tgz"
tar -czf $DST/backup-code.tgz $CODE
if [ $? -ne 0 ]; then
  echo "ERROR: something went wrong during backup..."
  exit 1
fi

echo "Updating codebase with git..."
cd $CODE

git stash
if [ $? -ne 0 ]; then
  echo "Error, bailing out"
  exit 1
fi

git checkout master
if [ $? -ne 0 ]; then
  echo "Error, bailing out"
  exit 1
fi

# Compare version before/after pulling files
currentVersion=`grep version package.json | awk '{ print $2 }' | sed 's/\"//g' | sed 's/,//'`

git pull | tee -a $TMP
if [ $? -ne 0 ]; then
  echo "Error, bailing out"
  exit 1
fi

grep -q "Already up to date." $TMP
if [ $? -eq 0 ]; then
  echo "Your Misskey code is already up to date, version:$currentVersion."
  rm -f $TMP
  exit 0
fi
rm -f $TMP

latestVersion=`grep version package.json | awk '{ print $2 }' | sed 's/\"//g' | sed 's/,//'`
echo "Upgrade needed, current:$currentVersion latest:$latestVersion."

git submodule update --init
if [ $? -ne 0 ]; then
  echo "Error, bailing out"
  exit 1
fi

git stash pop
if [ $? -ne 0 ]; then
  echo "Error, bailing out"
  exit 1
fi

echo "Building new images... (it will take some time)"
docker-compose --project-directory=$DATA --file $DATA/$COMPOSE build

#sudo docker-compose stop && sudo docker-compose up -d
if [ $AUTOUPDATE -eq 0 ]; then
  echo "Stop and restart the instance using:"
  echo "stop_misskey.sh"
  echo "start_misskey.sh"
else
  echo "Automatically updating the instance, fingers crossed"
  echo -n "Stopping..."
  stop_misskey.sh
  if [ $? -ne 0 ]; then
    echo "ERROR: something went wrong when stopping the instance"
    exit 1
  fi
  echo "done."
  echo -n "Starting..."
  start_misskey.sh
  if [ $? -ne 0 ]; then
    echo "ERROR: something went wrong when starting the instance"
    exit 1
  fi
  echo "done."
fi

exit 0
