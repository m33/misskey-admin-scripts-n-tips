#!/bin/bash
#
# Start misskey instance
# Rotate logs
#

### You will need to adjust Paths ###
# Store containers log
LOG=/var/log/docker
# Misskey (git) code directory
CODE=/usr/local/misskey/code
# Misskey (containers volumes) data directory
DATA=/usr/local/misskey/data
# Docker compose file name
COMPOSE=docker-compose.yml
###

DT=`date +%s`
mkdir -p $LOG || exit 1

# Rotate logs if needed on each (re)start, cleanup with tmpreaper or something
if [ -f $LOG/misskey.log ]; then
  mv $LOG/misskey.log $LOG/misskey.log.$DT
  nice gzip $LOG/misskey.log.$DT & 1>/dev/null 2>&1
fi

if [ ! -d $CODE ] || [ ! -d $DATA ]; then
  echo "Error, you may need to adjust path in this script"
  exit 1
fi

echo "[start_misskey] by $USER..." 1>$LOG/misskey.log
nohup docker-compose --project-directory=$DATA --file $DATA/$COMPOSE up 1>>$LOG/misskey.log 2>&1 &
exit $?
