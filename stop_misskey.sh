#!/bin/bash
#
# stop misskey instance using docker commands
#

### You will need to adjust Paths ###
# Store containers log
LOG=/var/log/docker/misskey.log
# Misskey (git) code directory
CODE=/usr/local/misskey/code
# Misskey (containers volumes) data directory
DATA=/usr/local/misskey/data
# Docker compose file name
COMPOSE=docker-compose.yml
###

if [ ! -d $CODE ] || [ ! -d $DATA ]; then
  echo "Error, you may need to adjust path in this script"
  exit 1
fi

echo "[stop_misskey] by $USER started..." >> $LOG
docker-compose  --project-directory=$DATA -f $DATA/$COMPOSE stop 1>>$LOG 2>&1
RV=$?
echo "[stop_misskey] done ($RV)" >> $LOG
exit $RV
