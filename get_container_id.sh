#!/bin/bash
#
# Get container ID from name
#
docker container ls|grep "$1"|awk '{print $1}'|grep -v CONTAINER
exit $?
