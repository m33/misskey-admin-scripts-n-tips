#!/bin/bash
#
# Prune all orphaned images, and stopped containers
#
docker container prune
docker image prune -a
exit $?
