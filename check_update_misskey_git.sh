#!/bin/bash
#
# Try to determine quickly if an update is due
# This script may trash your instance, use with caution, make backups...
#
# Return 0 if an upgrade is needed
#

### You will need to adjust Paths ###
# Store containers log
LOG=/var/log/docker/misskey.log
# Misskey (git) code directory
CODE=/usr/local/misskey/code
# Misskey (containers volumes) data directory
DATA=/usr/local/misskey/data
# Docker compose file name
COMPOSE=docker-compose.yml
COMPOSE=misskey-arm64.yml
# Backup destination directory
DST=/usr/local/backup
# Automatically stop, update and start the running instance
AUTOUPDATE=1
TMP=/tmp/.update_misskey.$$
###

if [ ! -d $CODE ] || [ ! -d $DATA ]; then
  echo "Error, you may need to adjust path in this script"
  exit 100
fi

cd $CODE
mv package.json package.json.$$

# Compare version before/after pulling files
currentVersion=`grep version package.json.$$ | awk '{ print $2 }' | sed 's/\"//g' | sed 's/,//'`

git checkout origin/master -- package.json
if [ $? -ne 0 ]; then
  echo "Error, bailing out"
  mv package.json.$$ package.json
  exit 100
fi

latestVersion=`grep version package.json | awk '{ print $2 }' | sed 's/\"//g' | sed 's/,//'`
rm -f package.json
mv package.json.$$ package.json

echo -n "Current:$currentVersion Latest:$latestVersion... "
if [ "$currentVersion" == "$latestVersion" ]; then
  echo "Your Misskey code is already up to date."
  exit 1
fi

echo "Upgrade needed."

exit 0
