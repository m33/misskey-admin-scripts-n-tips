# misskey-admin-scripts-n-tips

A collection of scripts and tips to make a Misskey admin's life a little bit easyer. 

It is a complement to the [official documentation](https://misskey-hub.net/en/docs/install/docker.html).

Abstract: a VPS with arm64 processor, Docker, a Misskey instance, what could save you some time ?

---
---

# Tips list: 

## Separate code and data

The default misskey docker install guide will place misskey code and your instance data in the same directory. Is something goes wrong on a later time, you may accidentally trash your datas (docker volumes mounted for the postgres database, redis cache...).

Separate the misskey code (git clone...) from you container's volumes, something like this should do:
```
/usr/local/misskey/code
/usr/local/misskey/data
```

You will need a custom yaml file for docker-compose, but it's worth it (see sample yaml file).

Manage your misskey instance containers with docker-compose and the right path for code and datas:
```
docker-compose --project-directory=/usr/local/misskey/data -f /usr/local/misskey/data/<your custom docker-compose>.yml 
```

## Quickly adjust misskey defaults settings

This particular setting in your instance configuration yaml in the `.config/default.yml` file will raises an error when federating with some (secured) mastodon instances, it's not trivial to find out, activate it to avoid such errors:
```
signToActivityPubGet: true
```

## Fill your global timeline with relays
Bootstraping a new instance, solo or small sized, is a real pain. You will see only content from people you follow. A solution to fill your TL is to register your instance to relays, that will repeat content from others instances to your TL.
Here is a list of popular relays:
```
https://relay.intahnet.co.uk/inbox

https://relay.pettingzoo.co/inbox

https://relay.blob.cat/inbox

https://relay.fedi.agency/inbox

https://relay.homunyan.com/inbox
```


## Arm64 specific

Deploying your misskey instance on arm64 using docker is pretty straightforward, you will need to use some generic flavor of docker images instead of specific/pined version from the original docker-compose.yml file (see the exemple yaml file in this repository).

---
---

# Scripts list:

## start_misskey.sh stop_misskey.sh
In a small VPS, or a limited computer like the rapsberry, or just because systemd, you may want to start and stop misskey with simple scripts like this.
For automatic start on boot, you may add this to your crontab: `@reboot /usr/local/bin/start_misskey.sh 1>>/var/log/syslog 2>&1`

## check_update_misskey_git.sh
This script tries to determine if an update is needed, without pulling all source code from Misskey's github depot.
Return 1 if an update is detected.

## update_misskey.sh
Handles the grunt work for a git update, and containers rebuild.
It takes care of code base backup, you really should take care of database (and maybe data files) backup before upgrading. To this day Misskey updates have not always been a smooth road.
It may automatically restart your instance to apply the update.

## backup_postgres.sh
A daily backup for your docker postgresql database, run it daily
It will cycle with the day of week (1..7)
Add it to your crontab, when your instances has its lowest activity, ex:
`0 5 * * * /usr/local/bin/backup_postgres.sh`

## cleanup_docker.sh
Prune all orphaned images, and stopped containers

## get_container_id.sh
Get container ID from name (used in other scripts)

---
---

# Ressources list:

## misskey-arm64.yml:

A docker-compose yaml file, for an arm64 (raspberry pi, VPS arm instances)
